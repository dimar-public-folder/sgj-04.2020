using UnityEngine;

public class RandomSound : MonoBehaviour
{
    [SerializeField] private AudioSource _source;
    [SerializeField] private AudioClip[] _clips;

    private void OnEnable()
    {
        _source.clip = _clips[Random.Range(0, _clips.Length)];
    }
}
