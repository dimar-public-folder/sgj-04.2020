using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ChatPersonView : MonoBehaviour, IPointerClickHandler
{
    public System.Action<Person> onClick;

    [SerializeField] private Image _avatarImage;
    [SerializeField] private Transform _alertView;

    private Person _pers;
    public Person Person => _pers;

    public void Init(Person pers)
    {
        _pers = pers;
        _avatarImage.sprite = pers.Avatar;
    }

    public void SetAlert(bool active = true)
    {
        _alertView.gameObject.SetActive(active);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        SetAlert(false);
        onClick?.Invoke(_pers);
    }
}
