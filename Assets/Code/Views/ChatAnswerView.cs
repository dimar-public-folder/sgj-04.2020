using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatAnswerView : MonoBehaviour
{
    public Action<string> onSelected;

    [SerializeField] private ChatMessageView _messagePrefab;
    [SerializeField] private Transform _container;

    private List<ChatMessageView> _messages = new List<ChatMessageView>();

    public ChatMessageView AddVariant(string msg)
    {
        var newView = Instantiate(_messagePrefab, _container);
        newView.Init(new ChatMessage(){text = msg, fromPerson = null});
        newView.onClick += OnMessageClick;
        // newView.transform.SetAsFirstSibling();
        _messages.Add(newView);

        return newView;
    }

    private void OnMessageClick(string message)
    {
        onSelected?.Invoke(message);
    }

    public void Clear()
    {
        for (int i = _messages.Count - 1; i >= 0 ; i--)
        {
            Destroy(_messages[i].gameObject);
        }

        _messages.Clear();
    }

    public void SetVariants(params string[] variants)
    {
        Clear();

        foreach (var variant in variants)
        {
            AddVariant(variant);
        }
    }
}
