using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChatPersonsView : MonoBehaviour
{
    public System.Action<Person> onSelected;

    [SerializeField] private ChatPersonView _personPrefab;
    [SerializeField] private Transform _container;
    
    private List<ChatPersonView> _persons = new List<ChatPersonView>();

    public void MessageAdded(ChatMessage message, bool alert)
    {
        var personView = _persons.FirstOrDefault(v => v.Person == message.fromPerson);

        if (personView == null)
        {
            personView = AddPerson(message.fromPerson);
        }

        personView.transform.SetAsFirstSibling();
        personView.SetAlert(alert);
    }

    public ChatPersonView AddPerson(Person pers)
    {
        var newPerson = Instantiate(_personPrefab, _container);
        newPerson.Init(pers);
        newPerson.onClick += OnPersonSelected;
        newPerson.transform.SetAsFirstSibling();
        _persons.Add(newPerson);

        return newPerson;
    }

    private void OnPersonSelected(Person pers)
    {
        // Debug.Log("person selected");
        onSelected?.Invoke(pers);
    }
}
