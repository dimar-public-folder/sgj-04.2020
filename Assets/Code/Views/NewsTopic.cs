using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class NewsTopic : MonoBehaviour
{
    [SerializeField] private Text _title;
    [SerializeField] private Text _text;
    [SerializeField] private Image _cover;
    [SerializeField] private Text _likesNum;
    [SerializeField] private AudioSource _source;
    
    public void Init(
        string title,
        string text,
        Sprite coverImage
    )
    {
        _text.text = text;
        _title.text = title;
        _cover.sprite = coverImage;

        var cv = GetComponent<CanvasGroup>();
        if (cv)
        {
            cv.alpha = 0;
            cv.DOFade(1, 1);
        }
    }

    public void SetLikes(int num, bool local, bool animated = true)
    {
        if (!animated)
        {
            _likesNum.text = num.ToString();
        }
        else
        {
            int startTotalLikes = LikesStorage.currentValue;
            int topicLikes = 0;
            var add = DOTween.To(
                () => topicLikes,
                (val) => {
                    if (!local) LikesStorage.currentValue = startTotalLikes + val;
                    topicLikes = val;
                    _likesNum.text = topicLikes.ToString();
                    // _source.Play();
                },
                num,
                20
            );
            var seq = DOTween.Sequence();
            seq.AppendInterval(3f);
            seq.Append(add);
        }
    }
}
