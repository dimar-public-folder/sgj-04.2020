using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatHistoryView : MonoBehaviour
{
    [SerializeField] private ChatMessageView _messagePrefab;
    [SerializeField] private Transform _container;
    [SerializeField] private ScrollRect _scroll;

    private List<ChatMessageView> _messages = new List<ChatMessageView>();

    public ChatMessageView AddMessage(ChatMessage msg, bool whileRead = false)
    {
        var newView = Instantiate(_messagePrefab, _container);
        newView.Init(msg, whileRead);
        // newView.transform.SetAsFirstSibling();
        _messages.Add(newView);
        StartCoroutine(_IEScrollBottom());

        return newView;
    }

    public void ClearHistory()
    {
        for (int i = _messages.Count - 1; i >= 0 ; i--)
        {
            Destroy(_messages[i].gameObject);
        }

        _messages.Clear();
    }

    public void SetHistory(ChatMessage[] messages)
    {
        ClearHistory();

        foreach (var msg in messages)
        {
            AddMessage(msg);
        }
    }

    private IEnumerator _IEScrollBottom()
    {
        yield return null;
        
        _scroll.normalizedPosition = new Vector2(0,0);
    }
}
