using UnityEngine;
using UnityEngine.UI;

public class LikesBarView : MonoBehaviour
{
    [SerializeField] private Slider _barSlider;

    private void Update()
    {
        _barSlider.value = LikesStorage.currentValue;
    }
}