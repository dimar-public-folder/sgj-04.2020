using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ChatWindow : MonoBehaviour
{
    public System.Action<string,Person> onAnswerSent;

    [SerializeField] private Image _persAvatar;
    [SerializeField] private Text _persName;
    [SerializeField] private Button _closeBtn;
    [SerializeField] private RectTransform _window;
    [SerializeField] private ChatHistoryView _history;
    [SerializeField] private ChatPersonsView _persList;
    [SerializeField] private ChatAnswerView _answers;
    [SerializeField] private AudioSource _source;

    private Person _currentAddressee = null;
    private Dictionary<Person,ChatHistory> _chats = new Dictionary<Person, ChatHistory>();
    // private Dictionary<Person,InsertionVariant> _chats;
    private bool _isOpen;

    private void Start()
    {
        _closeBtn.onClick.AddListener(OnCloseBtn);

        _answers.onSelected += OnAnswerSelected;
        _persList.onSelected += OnPersonSelected;

        UpdateOpenState(false);
    }

    private void OnPersonSelected(Person pers)
    {
        if (!_isOpen)
        {
            _isOpen = true;
            UpdateOpenState();
        }

        ShowChat(pers);

        _persAvatar.sprite = pers.Avatar;
        _persName.text = pers.Nickname;
    }

    private void OnAnswerSelected(string answer)
    {
        AddOutcome(_currentAddressee, answer);
    }

    private void OnCloseBtn()
    {
        _isOpen = !_isOpen;
        _currentAddressee = null;
        UpdateOpenState();
    }

    private void UpdateOpenState(bool animated = true)
    {
        if (_window.gameObject.activeSelf && !_isOpen)
        {// close
            var move = _window.DOLocalMoveX(275, animated ? 0.5f : 0f);
            move.OnComplete(() => _window.gameObject.SetActive(false));
        }
        else if (!_window.gameObject.activeSelf && _isOpen)
        {// open
            _window.gameObject.SetActive(true);
            var move = _window.DOLocalMoveX(-75f, animated ? 0.5f : 0f);
            // move.OnComplete(() => _window.gameObject.SetActive(true));
        }
    }

    public void AddIncome(Person sender, string message)
    {
        var newMsg = new ChatMessage(){fromPerson = sender, text = message};
        if (!_chats.ContainsKey(sender))
        {
            _chats[sender] = new ChatHistory();
        }
        _chats[sender].AddMessage(newMsg);

        if (_currentAddressee == sender && _isOpen)
            _history.AddMessage(newMsg, true);

        _persList.MessageAdded(newMsg, _currentAddressee != sender);

        _source.Play();
    }

    public void AddOutcome(Person addressee, string message)
    {
        var newMsg = new ChatMessage(){fromPerson = null, text = message};
        if (!_chats.ContainsKey(addressee))
        {
            _chats[addressee] = new ChatHistory();
        }
        _chats[addressee].AddMessage(newMsg);

        if (_currentAddressee == addressee && _isOpen)
            _history.AddMessage(newMsg, true);

        SetAnswerVariants(addressee);

        _source.Play();

        onAnswerSent?.Invoke(message, addressee);
    }

    public void SetAnswerVariants(Person addressee, params string[] variants)
    {
        if (!_chats.ContainsKey(addressee))
        {
            _chats[addressee] = new ChatHistory();
        }
        _chats[addressee].SetAnswers(variants);

        if (_currentAddressee == addressee)
            _answers.SetVariants(variants);
    }

    public void ShowChat(Person withPerson)
    {
        _currentAddressee = withPerson;
        
        _history.SetHistory(_chats[withPerson].Messages);
        _answers.SetVariants(_chats[withPerson].Answers);
    }
}
