using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Threading.Tasks;

public class BannerView : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private CanvasGroup _cv;

    private bool _isShowing = true;

    public IEnumerator Show(Sprite sprite, bool animated = true)
    {
        if (animated)
        {
            if (_isShowing)
            {
                yield return Hide();
            }

            _image.sprite = sprite;
            yield return new WaitForSeconds(0.5f);

            _cv.DOFade(1f,1f);
            yield return new WaitForSeconds(1f);
        }
        else
        {
            _cv.alpha = 1f;
        }

        yield break;
    }

    public IEnumerator Hide(bool animated = true)
    {
        if (animated)
        {
            _cv.DOFade(0f, 1f);
            yield return new WaitForSeconds(1f);
        }
        else
        {
            _cv.alpha = 0f;
        }

        yield break;
    }
}
