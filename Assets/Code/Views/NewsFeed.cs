using System.Collections;
using UnityEngine;

public class NewsFeed : MonoBehaviour
{
    [SerializeField] private NewsTopic _topicPrefab;
    [SerializeField] private NewsTopic _postPrefab;
    [SerializeField] private Transform _container;
    [SerializeField] private AudioSource _source;
    
    public NewsTopic AddTopic(
        string title,
        string text,
        Sprite coverImage
    )
    {
        var newTopic = Instantiate(_topicPrefab, _container);
        newTopic.Init(title, text, coverImage);
        newTopic.transform.SetAsFirstSibling();

        _source.Play();

        return newTopic;
    }

    public NewsTopic AddPost(
        Person poster,
        string text,
        int likes
    )
    {
        var newTopic = Instantiate(_postPrefab, _container);
        newTopic.Init(poster.Nickname, text, poster.Avatar);
        newTopic.SetLikes(likes, true);
        // StartCoroutine(IESetLikesWithDelay(3f, newTopic, likes));
        newTopic.transform.SetAsFirstSibling();

        _source.Play();

        return newTopic;
    }
}
