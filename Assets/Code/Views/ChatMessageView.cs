using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class ChatMessageView : MonoBehaviour, IPointerClickHandler
{
    public System.Action<string> onClick;
    [SerializeField] private Image _bubbleImage;
    [SerializeField] private Text _text;
    [SerializeField] private RectTransform _container;
    [SerializeField] private Sprite _fromElse;
    [SerializeField] private Sprite _fromMe;

    private string _displayedText;

    public void Init(ChatMessage msg, bool animated = false)
    {
        _displayedText = msg.text;
        _text.text = _displayedText;
        if (msg.fromPerson != null)
        {
            _bubbleImage.sprite = _fromElse;
            _text.alignment = TextAnchor.MiddleLeft;

            if (_container)
            {
                _container.pivot = new Vector2(0,0.5f);
                _container.anchorMin = new Vector2(0,_container.anchorMin.y);
                _container.anchorMax = new Vector2(0,_container.anchorMax.y);
                _container.anchoredPosition = new Vector2(0,_container.anchoredPosition.y);
            }
        }
        else
        {
            _bubbleImage.sprite = _fromMe;
            _text.alignment = TextAnchor.MiddleRight;

            if (_container)
            {
                _container.pivot = new Vector2(1f,0.5f);
                _container.anchorMin = new Vector2(1,_container.anchorMin.y);
                _container.anchorMax = new Vector2(1,_container.anchorMax.y);
                _container.anchoredPosition = new Vector2(0,_container.anchoredPosition.y);
            }
        }
        
        if (animated)
        {
            var cv = GetComponent<CanvasGroup>();
            if (cv)
            {
                cv.alpha = 0;
                cv.DOFade(1, 1);
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        onClick?.Invoke(_displayedText);
    }
}
