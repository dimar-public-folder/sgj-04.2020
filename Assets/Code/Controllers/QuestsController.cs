using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

public class QuestsController : MonoBehaviour
{
    public Action<Quest> onQuestFinished;
    public Action<Quest> onLastQuestFinished;

    [SerializeField] private Quest[] _quests;
    [SerializeField] private NewsFeed _feedView;
    [SerializeField] private DialogController _dialogController;

    [Space]
    [SerializeField] private float _questDelay = 2f;
    [SerializeField] private float _questDelayVariation = 0.5f;

    [Space]
    [SerializeField] private float _postDelay = 2f;
    [SerializeField] private float _postDelayVariation = 0.5f;

    [Space]
    [SerializeField] private float _reactionDelay = 2f;
    [SerializeField] private float _reactionDelayVariation = 0.5f;

    // private Task _currentQuest = null;
    private List<Quest> _unfinishedQuests;

    public void StartSeq()
    {
        // foreach (var quest in _quests)
        // {
        //     _feedView.AddTopic(
        //         quest.Topic.Title,
        //         quest.Topic.Text,
        //         null
        //     );
        // }
        _unfinishedQuests = new List<Quest>(_quests);
        onQuestFinished += OnQuestFinished;
        OnQuestFinished(null);
    }

    public void StopQuest()
    {
        // _currentQuest.Dispose();
        // _currentQuest = null;
    }

    public IEnumerator StartQuest(Quest quest)
    {
        // _currentQuest = quest;

        var questDelay = Random.Range(_questDelay-_questDelayVariation, _questDelay+_questDelayVariation);
        yield return new WaitForSeconds(questDelay);

        // диалог с заказчиком
        yield return _dialogController.StartDialog((Chat)quest.Order);

        // диалог с интервьюером
        if (quest.Interview)
            yield return _dialogController.StartDialog((Chat)quest.Interview);
        
        yield return _dialogController.StartInterview((TextInsertion[])quest.Topic.Insertions);
        // пост статьи
        var postDelay = Random.Range(_postDelay-_postDelayVariation, _postDelay+_postDelayVariation);
        yield return new WaitForSeconds(postDelay);
        var topic = _feedView.AddTopic(
            PostComposer.ComposePostTitle((Topic)quest.Topic),
            PostComposer.ComposePost((Topic)quest.Topic),
            CoverComposer.ComposeCover((Topic)quest.Topic)
        );
        var topicLikes = LikesCalc.Calc((Topic)quest.Topic);
        topic.SetLikes(topicLikes, false);

        // получение фидбека
        var reactionDelay = Random.Range(_reactionDelay-_reactionDelayVariation, _reactionDelay+_reactionDelayVariation);
        yield return new WaitForSeconds(reactionDelay);
        
        // _dialogController.StartDialog(_currentQuest.Feedback);
        var post = 
            topicLikes >= quest.GoodReactionMinLikes ?
            quest.GoodReaction : 
            quest.BadReaction;
        
        _feedView.AddPost(post.Poster, post.Text, post.Likes);

        // quest = null;

        onQuestFinished?.Invoke(quest);
    }

    private void OnQuestFinished(Quest finished)
    {
        _unfinishedQuests.Remove(finished);

        if (_unfinishedQuests.Count > 0)
        /*_currentQuest =*/ StartCoroutine(StartQuest(_unfinishedQuests[Random.Range(0,_unfinishedQuests.Count)]));
        else
            onLastQuestFinished?.Invoke(finished);
    }
}
