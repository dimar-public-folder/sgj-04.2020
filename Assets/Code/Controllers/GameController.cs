using System.Collections;
using System.Threading.Tasks;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] private QuestsController _questController;
    [SerializeField] private NewsFeed _feedView;
    [SerializeField] private AudioSource _source;

    [Space]
    [SerializeField] private Post _winPost;
    [SerializeField] private Post _losePost;
    [SerializeField] private Post _notWinPost;

    [Space]
    [SerializeField] private Transform _startScreen;
    [SerializeField] private Transform _winScreen;
    [SerializeField] private Transform _loseScreen;
    [SerializeField] private Transform _notWinScreen;

    [Header("Settings")]
    [SerializeField] private float _loadingTime = 3f;
    [SerializeField] private float _startDelay = 3f;
    [SerializeField] private float _finishDelay = 3f;

    private bool _gameFinished = false;
    
    private void Start()
    {
        _questController.onLastQuestFinished += OnLastQuestFinished;
        StartCoroutine(StartingSequence());
    }

    private void Update()
    {
        if (_gameFinished) return;

        if (LikesStorage.IsLose)
            StartCoroutine(FinishSequence(_losePost, _loseScreen));
        else if (LikesStorage.IsWon)
            StartCoroutine(FinishSequence(_winPost, _winScreen));
    }

    private IEnumerator StartingSequence()
    {
        _startScreen.gameObject.SetActive(true);

        yield return new WaitForSeconds(_loadingTime);

        _startScreen.gameObject.SetActive(false);

        yield return new WaitForSeconds(_startDelay);

        _questController.StartSeq();

        _source.Play();
    }

    private IEnumerator FinishSequence(Post finishPost, Transform finishScreen)
    {
        _gameFinished = true;

        _questController.StopQuest();

        _feedView.AddPost(finishPost.Poster, finishPost.Text, 0);

        yield return new WaitForSeconds(_finishDelay);

        finishScreen.gameObject.SetActive(true);
        _source.Stop();
    }

    private void OnLastQuestFinished(Quest finished)
    {
        StartCoroutine(FinishSequence(_notWinPost, _notWinScreen));
    }
}
