using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

public class DialogController : MonoBehaviour
{
    [SerializeField] private ChatWindow _chatView;
    [SerializeField] private float _questionDelay = 1f;
    [SerializeField] private float _questionDelayVariation = 0.5f;
    [SerializeField] private Person _interviewer;

    private Dictionary<Person,TextDialog> _dialogs = new Dictionary<Person, TextDialog>();
    private Dictionary<Person,string> _answers = new Dictionary<Person, string>();

    private void Start()
    {
        _chatView.onAnswerSent += OnAnswerReceived;
    }

    private void OnAnswerReceived(string answerText, Person addressee)
    {
        _answers[addressee] = answerText;
    }

    public IEnumerator StartDialog(Chat dialog)
    {
        if (!_dialogs.ContainsKey(dialog.Person))
        {
            // var sema = new SemaphoreSlim(0);
            yield return _IEDialog(dialog);
            // await sema.WaitAsync();
        }
        // _chatView.
    }

    public IEnumerator StartInterview(params TextInsertion[] insertions)
    {
        if (!_dialogs.ContainsKey(_interviewer))
        {
            // var sema = new SemaphoreSlim(0);
            yield return _IEInterview(insertions);
            // await sema.WaitAsync();
        }
    }

    private IEnumerator _IEDialog(Chat dialog)
    {
        _dialogs.Add(dialog.Person, dialog.Dialog);
        foreach (var dialogBlock in dialog.Dialog.Blocks)
        {
            _chatView.AddIncome(dialog.Person, dialogBlock.Question);

            if (dialogBlock.Answers.Length > 0)
            {
                _chatView.SetAnswerVariants(dialog.Person, dialogBlock.Answers);
                
                string answer;
                yield return new WaitUntil(() => _answers.TryGetValue(dialog.Person, out answer));

                _answers.Remove(dialog.Person);
            }

            var nextDelay = Random.Range(_questionDelay - _questionDelayVariation, _questionDelay + _questionDelayVariation);
            yield return new WaitForSeconds(nextDelay);
        }

        _dialogs.Remove(dialog.Person);
        // finishToken.Release();
    }

    private IEnumerator _IEInterview(TextInsertion[] insertions)
    {
        _dialogs.Add(_interviewer, null);
        foreach (var insertion in insertions)
        {
            var questionLines = insertion.Question.Split('\n');

            foreach (var line in questionLines)
            {
                var nextDelay = Random.Range(_questionDelay - _questionDelayVariation, _questionDelay + _questionDelayVariation);
                yield return new WaitForSeconds(nextDelay);
                
                _chatView.AddIncome(_interviewer, line);
            }

            if (insertion.Answers.Length > 0)
            {
                _chatView.SetAnswerVariants(_interviewer, insertion.Answers.Select(iv => iv.Answer).ToArray());
                
                string answer = "";
                yield return new WaitUntil(() => _answers.TryGetValue(_interviewer, out answer));
                insertion.SetAnswer(answer);

                _answers.Remove(_interviewer);
            }
        }

        _dialogs.Remove(_interviewer);
        // finishToken.Release();
    }
}
