public static class LikesCalc
{
    public static int Calc(Topic topic)
    {
        int summ = 0;
        foreach (var insrt in topic.Insertions)
        {
            if (insrt.SelectedInsertion != null)
                summ += insrt.SelectedInsertion.Likes;
        }

        return summ;
    }
}
