using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public static class PostComposer
{
    public static string ComposePost(Topic topic)
    {
        var resultText = topic.Text;
        var inserions = topic.Insertions;
        var searchPattern = "{(\\d),(\\d)}";
        var matches = Regex.Matches(topic.Text, searchPattern);

        // matches.
        for (int i = matches.Count - 1; i >= 0 ; i--)
        {
            var match = matches[i];
            
            var insertionIdx = int.Parse(match.Groups[1].Captures[0].Value) - 1;
            var textIdx = int.Parse(match.Groups[2].Captures[0].Value) - 1;

            var insertText = inserions[insertionIdx].SelectedInsertion.TextBlock[textIdx];

            var aStringBuilder = new StringBuilder(resultText);
            aStringBuilder.Remove(match.Index, match.Length);
            aStringBuilder.Insert(match.Index, insertText);
            resultText = aStringBuilder.ToString(); //match.Result(insertText);
        }

        return resultText;
    }

    public static string ComposePostTitle(Topic topic)
    {
        var resultText = topic.Title;
        var inserions = topic.Insertions;
        var searchPattern = "{(\\d),(\\d)}";
        var matches = Regex.Matches(topic.Title, searchPattern);

        // matches.
        for (int i = matches.Count - 1; i >= 0 ; i--)
        {
            var match = matches[i];
            
            var insertionIdx = int.Parse(match.Groups[1].Captures[0].Value) - 1;
            var textIdx = int.Parse(match.Groups[2].Captures[0].Value) - 1;

            var insertText = inserions[insertionIdx].SelectedInsertion.TextBlock[textIdx];

            var aStringBuilder = new StringBuilder(resultText);
            aStringBuilder.Remove(match.Index, match.Length);
            aStringBuilder.Insert(match.Index, insertText);
            resultText = aStringBuilder.ToString(); //match.Result(insertText);
        }

        return resultText;
    }
}
