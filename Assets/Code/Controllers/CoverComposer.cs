using System.Collections.Generic;
using UnityEngine;

public static class CoverComposer
{
    public static Sprite ComposeCover(Topic topic)
    {
        List<Sprite> layers = new List<Sprite>();
        foreach (var layer in topic.Cover.Layers)
        {
            if (!layer.VariantDependent)
            {
                layers.Insert(0, layer.Sprite);
            }
            else
            {
                layers.Insert(0, layer.Variants[topic.Insertions[layer.InsertIdx-1].SelectedInsertionIdx]);
            }
        }

        var tex = new Texture2D(480, 200);
        
        for(int x = 0; x < tex.width; x++)
        {
            for(int y = 0; y < tex.height; y++)
            {
                Color source = new Color(0,0,0,0);

                foreach (var spriteLayer in layers)
                {
                    var target = spriteLayer.texture.GetPixel(x,y);
                    // alpha blend when we've already written to the target
                    float sourceAlpha = source.a;
                    float invSourceAlpha = 1f - source.a;
                    float alpha = sourceAlpha + invSourceAlpha * target.a;
                    Color resultColor = (source * sourceAlpha + target * target.a * invSourceAlpha);// / alpha;
                    resultColor.a = alpha;
                    source = resultColor;
                }

                tex.SetPixel(x, y, source);
            }
        }
        tex.Apply();

        var result = Sprite.Create(tex, new Rect(0,0,tex.width,tex.height), Vector2.zero);
        return result;
    }
}
