using System;
using UnityEngine;

public class BannersController : MonoBehaviour
{
    [SerializeField] private BannerCollection _banners;
    [SerializeField] private BannerView[] _views;
    [SerializeField] private QuestsController _qc;

    private void Start()
    {
        // foreach (var v in _views)
        // {
        //     v.Hide(false);
        // }

        _qc.onQuestFinished += OnQuestFinished;
        OnQuestFinished(null);
    }

    private void OnQuestFinished(Quest finished)
    {
        foreach (var v in _views)
        {
            StartCoroutine(v.Show(_banners.GetRandom()));
        }
    }
}
