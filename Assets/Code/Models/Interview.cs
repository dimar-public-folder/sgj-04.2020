using UnityEngine;

[CreateAssetMenu(fileName = "Interview", menuName = "SGJ2020/Interview", order = 0)]
public class Interview : ScriptableObject
{
    [SerializeField] private Person person;
    [SerializeField] private TextDialog dialog;

    public Person Person => person;
    public TextDialog Dialog => dialog;
}
