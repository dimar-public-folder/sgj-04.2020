using UnityEngine;

[CreateAssetMenu(fileName = "TextDialog", menuName = "SGJ2020/TextDialog", order = 0)]
public class TextDialog : ScriptableObject
{
    [SerializeField] private DialogBlock[] blocks;

    public DialogBlock[] Blocks => blocks;

    // save answers
    // private void OnEnable()
    // {

    // }
}

[System.Serializable]
public class DialogBlock
{
    [TextArea]
    [SerializeField] private string question;
    [SerializeField] private string[] answers;

    public string Question => question;
    public string[] Answers => answers;
}
