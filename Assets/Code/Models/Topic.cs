using System;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Topic", menuName = "SGJ2020/Topic", order = 0)]
public class Topic : ScriptableObject
{
    [SerializeField] private string title;
    [TextArea]
    [SerializeField] private string text;
    [SerializeField] private TextInsertion[] insertions;
    [SerializeField] private TopicCover cover;

    public TextInsertion[] Insertions => insertions;
    public string Text => text;
    public string Title => title;
    public TopicCover Cover => cover;
}

[Serializable]
public class TextInsertion
{
    [TextArea]
    [SerializeField] private string question;
    [SerializeField] private InsertionVariant[] answers;

    public InsertionVariant[] Answers => answers;
    public string Question => question;
    public InsertionVariant SelectedInsertion { get {
        if (selectedAnswerIdx >= 0) return answers[selectedAnswerIdx];
        else return null;
    }}
    public int SelectedInsertionIdx => selectedAnswerIdx;


    private int selectedAnswerIdx = -1;

    public void SetAnswer(string selectedAnswer)
    {
        var selectedIdx = Array.FindIndex(answers, ins => ins.Answer == selectedAnswer);
        selectedAnswerIdx = selectedIdx;
    }
}

[Serializable]
public class InsertionVariant
{
    [SerializeField] private string answer;
    [SerializeField] private string[] textBlock;
    [SerializeField] private int likes;

    public string[] TextBlock => textBlock;
    public string Answer => answer;
    public int Likes => likes;
}

[Serializable]
public class TopicCover
{
    [SerializeField] private CoverLayer[] _layers;

    public CoverLayer[] Layers => _layers;
}

[Serializable]
public class CoverLayer
{
    [SerializeField] private bool variantDependent;
    [SerializeField] private Sprite sprite;
    [SerializeField] private int insertIdx;
    [SerializeField] private Sprite[] variants;

    public bool VariantDependent => variantDependent;
    public Sprite Sprite => sprite;
    public int InsertIdx => insertIdx;
    public Sprite[] Variants => variants;
}

// [Serializable]
// public class LayerVariant
// {
//     public int variantIdx;
//     public Sprite sprite;
// }