using UnityEngine;

[CreateAssetMenu(fileName = "Chat", menuName = "SGJ2020/Chat", order = 0)]
public class Chat : ScriptableObject
{
    [SerializeField] private Person person;
    [SerializeField] private TextDialog dialog;

    public Person Person => person;
    public TextDialog Dialog => dialog;
}
