using UnityEngine;

[CreateAssetMenu(fileName = "Person", menuName = "SGJ2020/Person", order = 0)]
public class Person : ScriptableObject
{
    [SerializeField] private Sprite avatar;
    [SerializeField] private string nickname;

    public string Nickname => nickname;
    public Sprite Avatar => avatar;
}
