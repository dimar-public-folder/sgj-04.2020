using UnityEngine;

[CreateAssetMenu(fileName = "BannerCollection", menuName = "SGJ2020/BannerCollection", order = 0)]
public class BannerCollection : ScriptableObject
{
    public Sprite[] banners;

    public Sprite GetRandom()
    {
        return banners[Random.Range(0,banners.Length)];
    }
}
