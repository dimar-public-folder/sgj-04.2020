using System.Collections.Generic;

public class ChatHistory
{
    private List<ChatMessage> _messages = new List<ChatMessage>();
    private List<string> _answers = new List<string>();

    public ChatMessage[] Messages => _messages.ToArray();
    public string[] Answers => _answers.ToArray();

    public void AddMessage(ChatMessage msg)
    {
        _messages.Add(msg);
    }

    public void SetAnswers(params string[] answers)
    {
        _answers.Clear();
        _answers.AddRange(answers);
    }

    public void Clear()
    {
        _messages.Clear();
        _answers.Clear();
    }
}
