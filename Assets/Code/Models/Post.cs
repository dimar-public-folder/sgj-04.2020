using UnityEngine;

[CreateAssetMenu(fileName = "Post", menuName = "SGJ2020/Post", order = 0)]
public class Post : ScriptableObject
{
    [SerializeField] private Person poster;
    [TextArea]
    [SerializeField] private string text;
    [SerializeField] private int likes;
    
    public int Likes => likes;
    public string Text => text;
    public Person Poster => poster;
}
