public static class LikesStorage
{
    public static int currentValue = 235;
    public static int minValue = 0;
    public static int maxValue = 2500;
    public static bool IsWon => currentValue >= maxValue;
    public static bool IsLose => currentValue <= minValue;
}