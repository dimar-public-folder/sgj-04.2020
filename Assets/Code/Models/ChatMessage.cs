public struct ChatMessage
{
    public Person fromPerson;
    public string text;
}