using UnityEngine;

[CreateAssetMenu(fileName = "Quest", menuName = "SGJ2020/Quest", order = 0)]
public class Quest : ScriptableObject
{
    [SerializeField] private Chat order;
    [SerializeField] private Chat interview;
    [SerializeField] private Topic topic;
    
    [SerializeField] private int goodReactionMinLikes;
    [SerializeField] private Post goodReaction;
    [SerializeField] private Post badReaction;

    public Chat Order => order;
    public Chat Interview => interview;
    public Topic Topic => topic;
    
    public int GoodReactionMinLikes => goodReactionMinLikes;
    public Post GoodReaction => goodReaction;
    public Post BadReaction => badReaction;
}
